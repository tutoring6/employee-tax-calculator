package joe;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author gorec
 */
public class MainClass {

	public static void main(String[] args) {

        List<Employee> employeeList = new ArrayList<Employee>();
        List<Department> departmentList = new ArrayList<Department>();

        int option;
        do {
            Scanner scanner = new Scanner(System.in);
            // User menu
            System.out.println("Please select your choice");
            System.out.println("1. Enter Department");
            System.out.println("2. Enter Employee");
            System.out.println("3. View Departments");
            System.out.println("4. View Employees");
            System.out.println("5. Sort Employees by Salary");
            System.out.println("6. Sort Employees by department");
            System.out.println("7. Employee with highest Salary");
            System.out.println("0. To quit");
            option = scanner.nextInt();
            switch (option) {
                case 1:
                    // Get department from user
                    Department department = getDepartmentInput();
                    department.setId(departmentList.size() + 1);
                    departmentList.add(department);

                    break;
                case 2:
                    Employee employee = getEmployeeInput(departmentList);
                    if (employee != null) {
                        employee.setId(employeeList.size() + 1);
                        employeeList.add(employee);
                    }
                    break;
                case 3:
                    printDepartments(departmentList);
                    break;
                case 4:
                    printEmployees(employeeList);
                    break;
                case 5:
                    //sort by salary and display employees
                    quickSortBySalary(employeeList,0,employeeList.size()-1);
                    System.out.println("Employees sorted By Salary...");
                    printEmployees(employeeList);
                    break;
                case 6:
                    //sort by salary and display employees
                    quickSortByDepartment(employeeList,0,employeeList.size()-1);
                    System.out.println("Employees sorted By department...");
                    printEmployees(employeeList);
                    break;
                case 7:
                    // find employee with highest salary
                    if(!employeeList.isEmpty()){
                        quickSortBySalary(employeeList,0,employeeList.size()-1);
                        Employee emp = employeeList.get(employeeList.size()-1);
                        System.out.println("Employee With highest Salary");
                        System.out.println(emp);
                        System.out.println("");
                    }
                    
                    break;
                case 0:
                    System.out.println("Exiting..");
                    break;
                default:
                    System.out.println("Invalid choice!");
                    break;
            }

        } while (option != 0);

    }

    private static void quickSortBySalary(List<Employee> employees, int p, int r) {
        if (p < r) {
            int q = partitionForSalary(employees, p, r);
            quickSortBySalary(employees, p, q - 1);
            quickSortBySalary(employees, q + 1, r);
        }
    }

    private static int partitionForSalary(List<Employee> employees, int b, int r) // partition method 
    {
        double pivot = employees.get(r).getGrossSalary();
        int pindex = b - 1;
        for (int i = b; i < r; i++) {
            if (employees.get(i).getGrossSalary() <= pivot) {
                pindex++;
                Employee e = employees.get(i);
                employees.set(i, employees.get(pindex));
                employees.set(pindex, e);
            }
        }
        pindex++;
        Employee e = employees.get(pindex);
        employees.set(pindex, employees.get(r));
        employees.set(r, e);
        return pindex;

    }

    private static void quickSortByDepartment(List<Employee> employees, int p, int r) {
        if (p < r) {
            int q = partitionForDepartment(employees, p, r);
            quickSortByDepartment(employees, p, q - 1);
            quickSortByDepartment(employees, q + 1, r);

        }

    }

    private static int partitionForDepartment(List<Employee> employees, int b, int r) // partition method 
    {
        int pivot = employees.get(r).getDepartmentId();
        int pindex = b - 1;
        for (int i = b; i < r; i++) {
            if (employees.get(i).getDepartmentId()<= pivot) {
                pindex++;
                Employee e = employees.get(i);
                employees.set(i, employees.get(pindex));
                employees.set(pindex, e);
            }
        }
        pindex++;
        Employee e = employees.get(pindex);
        employees.set(pindex, employees.get(r));
        employees.set(r, e);
        return pindex;

    }

    
    private static Department getDepartmentInput() {
        Scanner scanner = new Scanner(System.in);
        Department department = new Department();
        System.out.println("Enter department name: ");
        department.setName(scanner.nextLine());
        System.out.println("Enter department address: ");
        department.setAddress(scanner.nextLine());
        return department;
    }

    private static Employee getEmployeeInput(List<Department> departments) {
        Scanner scanner = new Scanner(System.in);
   
		Employee employee = new Employee();
		System.out.println("Enter Department ID: ");
		employee.setDepartmentId(scanner.nextInt());
		// validate departmentId
		boolean validDepartmentId = false;
		for (int i = 0; i<departments.size(); i++) {
			Department department = departments.get(i);
			if (employee.getDepartmentId() == department.getId()) {
				validDepartmentId = true;
				break;
			}
		}

		if(!validDepartmentId){
			System.out.println("Invalid Department ID");
			return null;
		}
		scanner.nextLine();
		System.out.println("Enter First Name: ");
		employee.setFirstName(scanner.nextLine());
		System.out.println("Enter Last Name: ");
		employee.setLastName(scanner.nextLine());
		System.out.println("Enter Gross Salary: ");
		employee.setGrossSalary(scanner.nextDouble());

		
		return employee;
   
    }
    
    private static void printEmployees(List<Employee> employees){
        System.out.println("------Employee List-----");
        for(Employee e: employees){
            System.out.println(e);
        }
        System.out.println("-----------------------------\n");
    }
    
    private static void printDepartments(List<Department> departments){
        System.out.println("------Department List-----");
        for(Department d: departments){
            System.out.println(d);
        }
        System.out.println("-----------------------------\n");
    }
}

class Employee {

    private int id;
    private int departmentId;
    private String firstName;
    private String lastName;
    private double grossSalary;
    private double tax;
    private double salaryAfterTax;

    public Employee() {
    }   

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public double getGrossSalary() {
        return grossSalary;
    }

    public void setGrossSalary(double grossSalary) {
        this.grossSalary = grossSalary;
        if (grossSalary < 12571) {
            tax = grossSalary * 0;
        } else if (grossSalary < 50271) {
            tax = (grossSalary - 12570) * 0.20;
        } else if (grossSalary < 150001) {
            tax = (grossSalary - 50270) * 0.40 + (50270 - 12570) * 0.20;
        } else {
            tax = (grossSalary - 150000) * 0.45 + (150000 - 37701) * 0.40 + (50270 - 12570) * 0.20;
        }
        salaryAfterTax = grossSalary - tax;
    }

    public int getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }

    public double getTax() {
        return tax;
    }

    public double getSalaryAfterTax() {
        return salaryAfterTax;
    }

    @Override
    public String toString() {
        return "id=" + id + ", departmentId=" + departmentId + ", firstName=" + firstName + ", lastName=" + lastName + ", grossSalary=" + grossSalary + ", tax=" + tax + ", salaryAfterTax=" + salaryAfterTax;
    }

}

class Department {

    private int id;
    private String name;
    private String address;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "id=" + id + ", name=" + name + ", address=" + address;
    }

}
